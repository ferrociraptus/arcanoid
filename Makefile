WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O0
CC = gcc
ADDICTIONAL_LIBS = `pkg-config --cflags --libs gtk+-3.0 cairo`
C_FLAGS = -I$(HEADER_DIR) -lm

SRC_DIR=src
BIN_DIR=bin
HEADER_DIR=head

SRC_FILES=$(wildcard $(SRC_DIR)/*.c)
HEADER_FILES=$(wildcard $(HEADER_DIR)/*.h)
OBJECTS=$(SRC_FILES:$(SRC_DIR)/%.c=$(BIN_DIR)/%.o)

EXECUTABLE = arkanoid

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(C_FLAGS) $(ADDICTIONAL_LIBS) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -o $@ $(OBJECTS)
	
$(OBJECTS): $(SRC_FILES) $(HEADER_FILES)
	$(CC) $(C_FLAGS) $(ADDICTIONAL_LIBS) $(WARNINGS) $(DEBUG) $(OPTIMIZE) -c -o $@ $(@:$(BIN_DIR)/%.o=$(SRC_DIR)/%.c)
	
clean:
	rm -f $(EXECUTABLE)
	rm $(OBJECTS)

run:
	./$(EXECUTABLE)
