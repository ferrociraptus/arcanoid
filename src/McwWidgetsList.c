#include "McwWidgetsList.h"
#include <stdlib.h>
#include "McwWidgetsListNode.h"

#define abs(A) (((A) < 0) ? -(A) : (A))

struct _mcw_widget_list{
    unsigned long len;
    unsigned long current_index;
    McwWidgetListNode* current_node;
};

static void __remove_all_nodes(McwWidgetList* list, McwWidgetListNode* node){
    if (node != list->current_node)
        __remove_all_nodes(list, node->next_node);
    mcw_widget_destroy(node->widget);
    free(node);
};

static void __go_to_index(McwWidgetList* list, unsigned index){
    int diff = list->current_index - index;
    list->current_index = index;
    for (int i = 0; i < abs(diff); i++)
        if (diff < 0)
            list->current_node = list->current_node->next_node;
        else
            list->current_node = list->current_node->previous_node;
}

McwWidgetList* mcw_widget_list_new(int nodes_amount){
    McwWidgetList* list = (McwWidgetList*)malloc(sizeof(McwWidgetList));
    if (list == NULL)
        return NULL;
    list->len = 0;
    list->current_index = 0;
    list->current_node = NULL;
    McwWidgetListNode *node;

    if (nodes_amount > 0){
        node = new_mcw_widget_list_node(NULL);
        if (node == NULL)
            return NULL;
        
        list->current_node = node;
        node->previous_node = node;
        node->next_node = node;
        list->len++;
        for (int i = 0; i < nodes_amount - 1; i++){
            node->next_node = new_mcw_widget_list_node(NULL);
            if (node->next_node == NULL){
                node->next_node = list->current_node;
                mcw_widget_list_destroy(list);
                return NULL;
            }
            node->next_node->previous_node = node;
            node = node->next_node;
            list->len++;
        }
        node->next_node = list->current_node;
        list->current_node->previous_node = node;
    }
    return list;
};

void mcw_widget_list_destroy(McwWidgetList* list){
    if (list == NULL)
        return;
    mcw_widget_list_clear(list);
    free(list);
}

void mcw_widget_list_clear(McwWidgetList* list){
    if (list->current_node != NULL)
        __remove_all_nodes(list, list->current_node->next_node);
    list->len = 0;
    list->current_index = 0;
    list->current_node = NULL;
}

int mcw_widget_list_insert(McwWidgetList* list, int index, McwWidget* mcw_widget){
    McwWidgetListNode* new_node = new_mcw_widget_list_node(mcw_widget);
    if (new_node == NULL || index < 0)
        return -1;
    if (index > list->len)
        index = list->len;
    
    if (list->len == 0){
        new_node->next_node = new_node;
        new_node->previous_node = new_node;
        list->current_node = new_node;
    }
    else{
        __go_to_index(list, index);
        new_node->next_node = list->current_node;
        new_node->previous_node = list->current_node->previous_node;
        new_node->previous_node->next_node = new_node;
        list->current_node->previous_node = new_node;
    }
    list->current_index++;
    list->len++;
    return 1;
}

int mcw_widget_list_append(McwWidgetList* list, McwWidget* mcw_widget){
    return mcw_widget_list_insert(list, list->len, mcw_widget);
}

McwWidget* mcw_widget_list_get(McwWidgetList* list, int index){
    __go_to_index(list, index);
    return list->current_node->widget;
}

void mcw_widget_list_set(McwWidgetList* list, int index, McwWidget* mcw_widget){
    __go_to_index(list, index);
    if (list->current_node->widget != NULL)
        mcw_widget_destroy(list->current_node->widget);
    list->current_node->widget = mcw_widget;
}

void mcw_widget_list_remove(McwWidgetList* list, int index){
    __go_to_index(list, index);
    McwWidgetListNode* del_node = list->current_node;
    del_node->previous_node->next_node = del_node->next_node;
    del_node->next_node->previous_node = del_node->previous_node;
    list->current_node = del_node->next_node;
    list->len--;
    list->current_index = list->current_index % list->len;
    mcw_widget_list_destroy_node(del_node);
}

unsigned long mcw_widget_list_len(McwWidgetList* list){
    return list->len;
}
