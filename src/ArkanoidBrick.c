#include <stdio.h>
#include "ArkanoidBrick.h"

void __brick_draw(Brick* brick, cairo_t* cr){
	
	static Point draw_point = {0, 0};
	
	if (point_is_zero(draw_point)){
		double x1, y1, x2, y2;
		cairo_t* img = cairo_create(brick->image);
		cairo_clip_extents(img, &x1, &y1, &x2, &y2);
		cairo_destroy(img);
		
		double height = x2 - x1;
		double width = y2 - y1;
		draw_point = point_create(
			brick->widget.center_position.x - height / 2,
			brick->widget.center_position.y - width / 2);
	}
	
    cairo_set_source_surface(cr, brick->image, draw_point.x, draw_point.y);
    cairo_paint(cr);
}

Brick* brick_new(BrickType type, Point center_pos){
    cairo_surface_t* surface;
    Brick* brick = (Brick*)malloc(sizeof(Brick));
    
    if (brick == NULL)
        return NULL;
    
    brick->widget = mcw_widget_create_base();
    brick->widget.destroy_widget = MCW_DESTROY_WIDGET_HANDLER(brick_destroy);
    brick->widget.draw_mywidget = MCW_WIDGET_DRAW_FUN(__brick_draw);
    
    brick->image = NULL;
    
    switch(type){
        case BRICK_USUAL:
            brick->lives = 1;
            surface = cairo_image_surface_create_from_png(BRICK_USUAL_IMAGE);
            break;
        case BRICK_STRONG:
            brick->lives = 3;
            surface = cairo_image_surface_create_from_png( BRICK_STRONG_FULL_IMAGE);
            break;
    }

//     double x1, y1, x2, y2;
//     cairo_t* cr = cairo_create(surface);
//     cairo_clip_extents(cr, &x1, &y1, &x2, &y2);
//     cairo_destroy(cr);
//     
//     brick->widget.size.height = x2 - x1;
//     brick->widget.size.width = y2 - y1;
    
    brick->widget.size.height = BRICK_HEIGHT; 
    brick->widget.size.width = BRICK_WIDTH;
    
    brick->widget.center_position = center_pos;
    brick->type = type;
    brick->image = surface;
    
    return brick;
}

void brick_destroy(Brick* brick){
    if (brick == NULL)
        return;
//     if (brick->image != NULL)
//         cairo_surface_destroy(brick->image);
    free(brick);
    
}

void brick_hit(Brick* brick){
    brick->lives--;
    cairo_surface_t* surface;
    
    switch (brick->lives){
            case 2:
                surface = cairo_image_surface_create_from_png(BRICK_STRONG_DAMAGED_IMAGE);
                break;
            case 1:
                surface = cairo_image_surface_create_from_png(BRICK_STRONG_RUINS_IMAGE);
                break;
        }
    cairo_surface_destroy(brick->image);
    brick->image = surface;
}

int brick_is_broken(Brick* brick){
    if (brick->lives <= 0)
        return 1;
    return 0;
}
