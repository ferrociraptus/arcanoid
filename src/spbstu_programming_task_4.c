#include <stdio.h>
#include "mcw.h"
#include "ArkanoidGame.h"

void end_game_handler();

void easy_button_handler(McwWidget button, void* data){
    mcw_system_close_ui();
    mcw_system_new_ui();
    arkanoid_game_init(LIGHT_GAME_LVL);
    arkanoid_game_set_end_game_handler(end_game_handler);
}
void medium_button_handler(McwWidget button, void* data){
    mcw_system_close_ui();
    mcw_system_new_ui();
    arkanoid_game_init(MEDIUM_GAME_LVL);
    arkanoid_game_set_end_game_handler(end_game_handler);
}
void hard_button_handler(McwWidget button, void* data){
    mcw_system_close_ui();
    mcw_system_new_ui();
    arkanoid_game_init(HARD_GAME_LVL);
    arkanoid_game_set_end_game_handler(end_game_handler);
}

void start_button_handler(McwWidget button, void* data){
    mcw_system_close_ui();
    mcw_system_load_ui("style_sheets/Set_complexity.mcw");
    
    McwWidget* easy_b;
    McwWidget* medium_b;
    McwWidget* hard_b;
    easy_b = mcw_system_get_widget("easy");
    medium_b = mcw_system_get_widget("medium");
    hard_b = mcw_system_get_widget("hard");
    
    mcw_widget_connect_signal_handler(&easy_b->signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(easy_button_handler), NULL);
    mcw_widget_connect_signal_handler(&medium_b->signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(medium_button_handler), NULL);
    mcw_widget_connect_signal_handler(&hard_b->signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(hard_button_handler), NULL);
}

void end_button_handler(McwWidget button, void* data){
    mcw_system_close_ui();
    mcw_system_close();
}

void load_init_window(){
    mcw_system_load_ui("style_sheets/Init_window.mcw");
    
    McwWidget* start_button = mcw_system_get_widget("start");
    mcw_widget_connect_signal_handler(&start_button->signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(start_button_handler), NULL);
    
    McwWidget* end_button = mcw_system_get_widget("exit");
    mcw_widget_connect_signal_handler(&end_button->signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(end_button_handler), NULL);
}

void end_game_handler(){
    mcw_system_close_ui();
    load_init_window();
}


void activate(){
    load_init_window();
}

int main(int argc, char** argv){
    mcw_system_connect_init_function(activate);
    mcw_system_run(argc, argv);
    mcw_system_close();
    return 0;
}
