#include "PointStruct.h"

Point point_create(double x, double y){
    return (Point){x, y};
}

Point point_sum(Point point1, Point point2){
    Point ans;
    ans.x = point1.x + point2.x;
    ans.y = point1.y + point2.y;
    return ans;
}

Point point_reduce(Point point1, Point point2){
    Point ans;
    ans.x = point1.x - point2.x;
    ans.y = point1.y - point2.y;
    return ans;
}

int point_is_equal(Point point1, Point point2){
    if (point1.x == point2.x && point1.y == point2.y)
        return 1;
    return -1;
}

int point_is_zero(Point point){
    if (point.x == 0.0 && point.y == 0.0)
        return 1;
    return -1;
}
