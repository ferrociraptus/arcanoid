#include "McwWidgetsListNode.h"
#include <stdlib.h>
#include "McwWidget.h"


McwWidgetListNode* new_mcw_widget_list_node(McwWidget* widget) {
    McwWidgetListNode *node = (McwWidgetListNode*) malloc(sizeof(McwWidgetListNode));
    if (node == NULL)
        return NULL;
    node->widget = widget;
    return node;
}

void mcw_widget_list_destroy_node(McwWidgetListNode *node){
    if (node == NULL)
        return;
    mcw_widget_destroy(node->widget);
    free(node);
}
