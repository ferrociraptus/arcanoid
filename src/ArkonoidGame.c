#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <cairo.h>
#include <math.h>

#include "ArkanoidGame.h"

#include "VoidList.h"
#include "McwSystem.h"
#include "Timers.h"

#include "ArkanoidBall.h"
#include "ArkanoidBrick.h"
#include "ArkanoidPlatform.h"
#include "ArkanoidBonus.h"

#define BALL_RADIUS 8
#define BLOCK_ROWS_AMOUNT 7
#define BLOCKS_IN_ROW_AMOUNT 10
#define BLOCKS_POSITION_SHIFT -1.5

#define BONUSES_DROP_SPEED 1

static int update_game_tiks = 10;

typedef struct{
    McwWidget widget;
	ArkanoidGameComplexity complexity;
    VoidList* bricks_rows;
    VoidList* balls;
	VoidList* bonuses;
    Platform* platform;
    unsigned lives;
    unsigned score;
    unsigned pause :1;
    void (*end_game_handler)();
    void (*pause_handler)();
} ArkanoidGame;

static ArkanoidGame game;

void __draw_arcanoid_game(void* widget, cairo_t* cr){
    
    cairo_font_extents_t fe;
    cairo_text_extents_t te;
    
    for (unsigned i = 0; i < void_list_len(game.bricks_rows); i++){
        VoidList* list = void_list_get(game.bricks_rows, i);
        for (unsigned k=0; k < void_list_len(list); k++){
            mcw_widget_draw(void_list_get(list, k), cr);
        }
    }
    
    for (unsigned i = 0; i < void_list_len(game.balls); i++)
        mcw_widget_draw(void_list_get(game.balls, i), cr);
    
    mcw_widget_draw(MCW_WIDGET(game.platform), cr);
	
	for (int i = 0; i < void_list_len(game.bonuses); i++)
		mcw_widget_draw(void_list_get(game.bonuses, i), cr);
    
    char game_lives[50];
    char game_score[50];
    
    sprintf(game_lives, "Lives: %d", game.lives);
    sprintf(game_score, "Score: %d", game.score);
    
    
    cairo_set_source_rgb(cr, 0, 0, 0.7);
    cairo_set_font_size(cr, 20);
    cairo_select_font_face(cr, "Purisa",
      CAIRO_FONT_SLANT_NORMAL,
      CAIRO_FONT_WEIGHT_BOLD);
    cairo_font_extents (cr, &fe);
    cairo_text_extents(cr, game_lives, &te);
    cairo_move_to (cr, 5, fe.height - 3);
    cairo_show_text (cr, game_lives);
    cairo_text_extents(cr, game_score, &te);
    cairo_move_to (cr, 5, fe.height + 20);
    cairo_show_text (cr, game_score);
    
    if (game.pause){
        Point back_right_point = point_create(game.widget.center_position.x + game.widget.size.width/2, game.widget.center_position.y + game.widget.size.height/2);
        cairo_rectangle(cr, 0, 0, back_right_point.x, back_right_point.y);
        cairo_set_source_rgba(cr, 0.3, 0.3, 0.3, 0.8);
        cairo_fill(cr);
        
        int brick_amount = 0;
        for (int i = 0; i < void_list_len(game.bricks_rows); i++)
            brick_amount += void_list_len(void_list_get(game.bricks_rows, i));

        if (brick_amount == 0){
            cairo_set_source_rgb(cr, 0.9, 0.5, 0);
            cairo_set_font_size(cr, 55);
            
            cairo_font_extents (cr, &fe);
            cairo_text_extents(cr, "You win!", &te);
            cairo_move_to (cr, game.widget.center_position.x - te.x_bearing - te.width / 2,
                    game.widget.center_position.y - fe.descent + fe.height / 2);
            cairo_show_text (cr, "You win!");
        }
        else if (game.lives > 0){
            cairo_set_source_rgb(cr, 0.15, 0.15, 0.15);
            cairo_set_font_size(cr, 15);
            cairo_font_extents (cr, &fe);
            cairo_text_extents(cr, "(click to resume)", &te);
            cairo_move_to (cr, game.widget.center_position.x - te.x_bearing - te.width / 2, game.widget.center_position.y - fe.descent + fe.height / 2 + 30);
            cairo_show_text (cr, "(click to resume)");
        }
        else{
            cairo_set_source_rgb(cr, 0.9, 0.5, 0);
            cairo_set_font_size(cr, 50);
            
            cairo_font_extents (cr, &fe);
            cairo_text_extents(cr, "Game over", &te);
            cairo_move_to (cr, game.widget.center_position.x - te.x_bearing - te.width / 2,
                    game.widget.center_position.y - fe.descent + fe.height / 2);
            cairo_show_text (cr, "Game over");
        }
        
//         cairo_stroke(cr);
    }
}

static HitType __if_ball_hit_widget(Point ball_pos, double radius, McwWidget* widget){
    struct{
        Point tl_point; //top left point
        Point tr_point; //top right point
        Point br_point; //bottom right point
        Point bl_point; //bottom left point
    } rect = {
        .tl_point = point_create(widget->center_position.x - widget->size.width/2, widget->center_position.y - widget->size.height/2),
        
        .tr_point = point_create(widget->center_position.x + widget->size.width/2, widget->center_position.y - widget->size.height/2),
        
        .br_point = point_create(widget->center_position.x + widget->size.width/2, widget->center_position.y + widget->size.height/2),
        
        .bl_point = point_create(widget->center_position.x - widget->size.width/2, widget->center_position.y + widget->size.height/2)
    };
    
    if (rect.tl_point.x - 1<= ball_pos.x && ball_pos.x <= rect.tr_point.x + 1)
        if((radius + widget->size.height / 2 > fabs(ball_pos.y - widget->center_position.y)))
            return HORISONTAL_HIT;
    
    if (rect.tl_point.y < ball_pos.y && ball_pos.y < rect.bl_point.y)
        if((radius + widget->size.width / 2 -1 > fabs(ball_pos.x - widget->center_position.x)))
            return VERTICAL_HIT;

    return NO_HIT;
}

void __update_arcanoid_game(){
	for (int i = 0; i < void_list_len(game.balls); i ++){
		Ball* ball = void_list_get(game.balls, i);
		arkanoid_ball_update_position(ball);
		double ball_rad = arkanoid_ball_get_radius(ball);
		McwWidget* ball_widget = MCW_WIDGET(ball);
        
		// check if ball placed into window
		if (game.widget.size.width - ball_widget->center_position.x < ball_rad ||
			ball_widget->center_position.x < ball_rad)
			arkanoid_ball_hit_handler(ball, VERTICAL_HIT, rvector_create(0,0));
        
		if (ball_widget->center_position.y < ball_rad)
			arkanoid_ball_hit_handler(ball, HORISONTAL_HIT, rvector_create(0,0));
        
		if (game.widget.size.height - ball_widget->center_position.y < ball_rad){
			if (void_list_len(game.balls) == 1){
				game.lives--;
				arkanoid_ball_set_position(ball, point_create(game.widget.size.width *0.5, game.widget.size.height * 0.6));
				RVector move_vector = rvector_create((double)rand()/RAND_MAX*2.0-1.0,
											(double)rand()/RAND_MAX*2.0-1.0);
				arkanoid_ball_set_move_vector(ball, move_vector);
				arkanoid_game_pause();
			}
			else{
				void_list_remove(game.balls, i--);
				continue;
			}
		}

		srand(time(NULL));
		McwWidget* ball_w = MCW_WIDGET(ball);
		arkanoid_ball_hit_handler(ball, __if_ball_hit_widget(ball_w->center_position, arkanoid_ball_get_radius(ball), MCW_WIDGET(game.platform)), rvector_create((double)rand()/RAND_MAX*10.0-5, (double)rand()/RAND_MAX*10.0-10.0));

		for (int i = void_list_len(game.bricks_rows) - 1; i >= 0; i--){
			VoidList* row = void_list_get(game.bricks_rows, i);
			for (int k = 0; k < void_list_len(row); k++){
				Brick* brick = void_list_get(row, k);
				HitType hit = __if_ball_hit_widget(ball_w->center_position, arkanoid_ball_get_radius(ball), MCW_WIDGET(brick));
			arkanoid_ball_hit_handler(ball, hit, rvector_create(0,0));
				if (hit != NO_HIT){
					brick_hit(brick);
					if (brick_is_broken(brick)){
						switch (brick->type){
							case BRICK_USUAL:
								game.score++;
								break;
							case BRICK_STRONG:
								game.score += 5;
								break;
						}
						//bonus creation
						double rand_val = (double)rand()/RAND_MAX*100.0;
						int is_bonus = -1;
						switch(game.complexity){
							case LIGHT_GAME_LVL:
								if (rand_val < 15.0)
									is_bonus = 1;
								break;
							case MEDIUM_GAME_LVL:
								if (rand_val < 10.0)
									is_bonus = 1;
								break;
							case HARD_GAME_LVL:
								if (rand_val < 10.0)
									is_bonus = 1;
								break;
						}
						if (is_bonus == 1){
							Bonus* bonus;
							rand_val = (double)rand()/RAND_MAX-0.5;
							
							if (rand_val > 0)
								bonus = bonus_new(INCREASE_PLATFORM_LEN, BONUSES_DROP_SPEED);
							else
								bonus = bonus_new(CREATE_NEW_BALL, BONUSES_DROP_SPEED);
							
							bonus->widget.center_position = brick->widget.center_position;
							void_list_append(game.bonuses, bonus);
						}
						void_list_remove(row, k--);
					}
				}
			}
		}
        
        arkanoid_ball_update_position(ball);
    }
    
    for (int i = 0; i < void_list_len(game.bonuses); i++){
			Bonus* bonus = void_list_get(game.bonuses, i);
			bonus_update_position(bonus);
			HitType hit = __if_ball_hit_widget(bonus->widget.center_position, BONUS_RADIUS, MCW_WIDGET(game.platform));
			if (hit != NO_HIT){
				bonus_activate(bonus);
				void_list_pop(game.bonuses, i);
				i--;
			}
		}
    
	if (game.lives == 0)
        arkanoid_game_pause();
    
    int brick_amount = 0;
    for (int i = 0; i < void_list_len(game.bricks_rows); i++)
        brick_amount += void_list_len(void_list_get(game.bricks_rows, i));
    
    if (brick_amount == 0)
        arkanoid_game_pause();
    
    timer_init_new("game", update_game_tiks, __update_arcanoid_game, NULL);
}

void __on_mouse_move(McwWidget* widget, Point* point){
	McwWidget* platform_w = MCW_WIDGET(game.platform);
    if (point->x < MIN_WINDOW_SIZE_WEIGHT_PX - ((double)platform_w->size.width) / 2 &&
        point->x > ((double)platform_w->size.width) / 2)
        mcw_set_widget_center_position(MCW_WIDGET(game.platform), (Point){point->x, PLATFORM_HEIGHT_POSITION});
}

void __arkanoid_game_destroy(){
    void_list_destroy(game.bricks_rows);
    void_list_destroy(game.balls);
}

void __arcanoid_game_click_handler(McwWidget* widget, void* data){
    
    int brick_amount = 0;
    for (int i = 0; i < void_list_len(game.bricks_rows); i++)
        brick_amount += void_list_len(void_list_get(game.bricks_rows, i));
    
    if (game.lives != 0 && brick_amount != 0)
        arkanoid_game_resume();
    else
        (*game.end_game_handler)();
}

void __arcanoid_game_activate_bonuse(ArkanoidBonusType type){
	RVector move_vector;
	ColorRange color_range;
	Ball* ball;
	Point ball_pos;
	
	McwWidget* platform_w = MCW_WIDGET(game.platform);
	
	switch (type){
		case INCREASE_PLATFORM_LEN:
			platform_make_long(game.platform);
			break;
		case CREATE_NEW_BALL:
			ball_pos = point_create(platform_w->center_position.x, platform_w->center_position.y - PLATFORM_HEIGT);
			move_vector = rvector_create((double)rand()/RAND_MAX,
                                       (double)rand()/RAND_MAX);
			move_vector = rvector_set_len(move_vector, 4);
			color_range = color_range_create(color_create(0,0,1),
                                         color_create(0,1,1));
			ball = arkanoid_ball_new(7, move_vector, ball_pos, color_range, BALL_RADIUS, 0.5);
			void_list_append(game.balls, ball);
			break;
	}
}

void __arcanoid_game_deactivate_bonuse(ArkanoidBonusType type){
	if(type == INCREASE_PLATFORM_LEN){
		platform_make_normal(game.platform);
	}
}

void arkanoid_game_init(ArkanoidGameComplexity complexity){
	Ball* init_ball;
    
	mcw_system_close_ui();
    mcw_system_new_ui();
	
	game.complexity = complexity;
	
    game.widget = mcw_widget_create_base();
    mcw_widget_set_size(&game.widget, mcw_widget_get_size(mcw_system_get_widget("window")));
    game.widget.destroy_widget = MCW_DESTROY_WIDGET_HANDLER(__arkanoid_game_destroy);
    
    game.balls = void_list_new(1, DESTRUCTOR(mcw_widget_destroy));
    
    double hard_persentage;
    switch(complexity){
        case LIGHT_GAME_LVL:
            update_game_tiks = 13;
            hard_persentage = 0.0;
            break;
        case MEDIUM_GAME_LVL:
            update_game_tiks = 10;
            hard_persentage = 0.3;
            break;
        case HARD_GAME_LVL:
            update_game_tiks = 7;
            hard_persentage = 0.6;
    }
    Point ball_pos = point_create(game.widget.size.width * 0.5, game.widget.size.height * 0.6);
    RVector move_vector = rvector_create((double)rand()/RAND_MAX*2.0-1.0,
                                       (double)rand()/RAND_MAX*2.0-1.0);
    move_vector = rvector_set_len(move_vector, 4);
    ColorRange color_range = color_range_create(color_create(0,0,1),
                                         color_create(0,1,1));
    init_ball = arkanoid_ball_new(7, move_vector, ball_pos, color_range, BALL_RADIUS, 0.5);
    mcw_widget_set_parent(MCW_WIDGET(init_ball), &game.widget);
    game.balls = void_list_new(1, DESTRUCTOR(mcw_widget_destroy));
    void_list_set(game.balls, 0, init_ball);
    
    Point brick_pos = {0, (double)BRICK_HEIGHT / 2};
        
    srand(time(NULL));
    game.bricks_rows = void_list_new(BLOCK_ROWS_AMOUNT, DESTRUCTOR(void_list_destroy));
    for (unsigned i = 0; i < void_list_len(game.bricks_rows); i++){
        
        brick_pos.x = (double)BRICK_WIDTH / 2;
        
        VoidList* row = void_list_new(BLOCKS_IN_ROW_AMOUNT, DESTRUCTOR(mcw_widget_destroy));
        void_list_set(game.bricks_rows, i, row);
        
        for (unsigned k = 0; k < void_list_len(row); k++){
            Brick* brick;
            if ((double)rand()/RAND_MAX < hard_persentage)
                brick = brick_new(BRICK_STRONG, brick_pos);
            else
                brick = brick_new(BRICK_USUAL, brick_pos);
            void_list_set(row, k, brick);
//             mcw_widget_set_parent(MCW_WIDGET(brick), &game.widget);
            brick_pos.x += (double)BRICK_WIDTH + BLOCKS_POSITION_SHIFT;
        }
        brick_pos.y += (double)BRICK_HEIGHT + BLOCKS_POSITION_SHIFT;
    }
    
    game.bonuses = void_list_new(0, DESTRUCTOR(mcw_widget_destroy));
	bonus_set_activator(__arcanoid_game_activate_bonuse);
	bonus_set_deactivator(__arcanoid_game_deactivate_bonuse);
    
    game.platform = platform_new();
    
    game.lives = 3;
    game.score = 0;
    game.pause = 1;
    
    game.end_game_handler = mcw_empty_handler;
    
    game.widget.draw_mywidget = MCW_WIDGET_DRAW_FUN(__draw_arcanoid_game);
    mcw_widget_connect_signal_handler(&game.widget.__system_calls.mouse_moved, MCW_WIDGET_SIGNALS_HANDLER(__on_mouse_move) ,NULL);
    mcw_widget_connect_signal_handler(&game.widget.__system_calls.clicked, MCW_WIDGET_SIGNALS_HANDLER(__arcanoid_game_click_handler), NULL);
    mcw_system_add_widget(&game.widget);
    game.widget.size = game.widget.parent->size;
    game.widget.center_position = game.widget.parent->center_position;
	
// 	timer_activate();
    timer_destroy_all();
    timer_init_new("game", update_game_tiks, __update_arcanoid_game, NULL);
    arkanoid_game_pause();
}

McwWidget* arkanoid_game_get_game_widget(){
    return &game.widget;
}

void arkanoid_game_pause(){
    timer_block();
    game.pause = 1;
}

void arkanoid_game_resume(){
    timer_activate();
    game.pause = 0;
}

void arkanoid_game_set_end_game_handler(void (*handler)()){
    game.end_game_handler = handler;
}

