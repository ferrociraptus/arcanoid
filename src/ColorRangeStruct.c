#include "ColorRangeStruct.h"

ColorRange color_range_create(ColorRGB first, ColorRGB second){
    return (ColorRange){first, second, 0};
}

void color_range_set_range(ColorRange* color_range, unsigned range){
    color_range->range = range - 1;
}

ColorRGB color_range_get_val(ColorRange range, unsigned iter){
    ColorRGB color_step = color_reduce(range.second, range.first);
    color_step = color_multiply(color_step, ((double)1.0)/range.range*iter);
    return color_sum(range.first, color_step);
}
