#include <stdlib.h>
#include <string.h>
#include "Timers.h"
#include "VoidList.h"

static int __timer_blocked;

typedef struct{
    char* indificator;
    unsigned long tiks;
    TimerHandler handler;
    void* data;
} Timer;

static VoidList* timers = NULL;

void __timer_destroy(Timer* timer){
    if (timer == NULL)
        return;
    
    if (timer->indificator != NULL)
        free(timer->indificator);
    
    free(timer);
}

static Timer* __timer_get_by_id(const char* indificator){
    for (unsigned i = 0; i < void_list_len(timers); i++){
        Timer* timer = (Timer*)void_list_get(timers, i);
        if (strcmp(indificator, timer->indificator) == 0){
			timer_activate();
            return timer;
        }
    }
    return NULL;
}

signed timer_init_new(char* indificator, unsigned long tiks, TimerHandler handler, void* data){
    if (timers == NULL){
        if ((timers = void_list_new(0, DESTRUCTOR(__timer_destroy))) == NULL)
            return -1;
    }

    Timer* new_timer = (Timer*)malloc(sizeof(Timer));
    if (new_timer == NULL)
        return -1;

    int id_len = strlen(indificator);
    new_timer->indificator = (char*)malloc((id_len + 1) * sizeof(char));
    strcpy(new_timer->indificator, indificator);

    new_timer->tiks = tiks;
    new_timer->handler = handler;
    new_timer->data = data;

    return void_list_append(timers, new_timer);
}

void timer_tick(){
    if (__timer_blocked > 0)
        return;
    if (timers == NULL)
        return;
    
    for (unsigned i = 0; i < void_list_len(timers); i++){
        Timer* timer = (Timer*)void_list_get(timers, i);
        timer->tiks--;
        if (timer->tiks == 0){
            (*timer->handler)(timer->data);
            void_list_remove(timers, i--);
        }
    }
}

void timer_destroy_all(){
    void_list_destroy(timers);
	timers = NULL;
}

int timer_set_time(const char* indificator, unsigned tiks){
    Timer* timer = __timer_get_by_id(indificator);
    if (timer == NULL)
		return -1;
	
	timer->tiks = tiks;
	return 1;
}

int timer_add_time(const char* indificator, unsigned tiks){
    Timer* timer = __timer_get_by_id(indificator);
    if (timer == NULL)
		return -1;
	
	timer->tiks += tiks;
	return 1;
}

void timer_block(){
    __timer_blocked = 1;
}

int timer_is_active(){
    return !__timer_blocked;
}

void timer_activate(){
    __timer_blocked = -1;
}
