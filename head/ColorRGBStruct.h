#ifndef COLOR_RGB
#define COLOR_RGB


typedef struct{
    double red;
    double green;
    double blue;
}ColorRGB;

ColorRGB color_create(double red, double green, double blue);
ColorRGB color_multiply(ColorRGB color, double val);
ColorRGB color_sum(ColorRGB color1, ColorRGB color2);
ColorRGB color_reduce(ColorRGB color1, ColorRGB color2);


#endif
