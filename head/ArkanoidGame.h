#ifndef ARKANOID_GAME_H
#define ARKANOID_GAME_H

#include "McwWidget.h"

typedef enum {LIGHT_GAME_LVL, MEDIUM_GAME_LVL, HARD_GAME_LVL} ArkanoidGameComplexity;

void arkanoid_game_init(ArkanoidGameComplexity complexity);
McwWidget* arkanoid_game_get_game_widget();
void arkanoid_game_pause();
void arkanoid_game_resume();
void arkanoid_game_set_end_game_handler(void (*handler)());

#endif
