#ifndef POINT_LIST_H
#define POINT_LIST_H

#include "McwWidget.h"

typedef struct _mcw_widget_list McwWidgetList;

McwWidgetList* mcw_widget_list_new(int nodes_amount);
void mcw_widget_list_destroy(McwWidgetList* list);

void mcw_widget_list_clear(McwWidgetList* list);
int mcw_widget_list_insert(McwWidgetList* list, int index, McwWidget* mcw_widget);
int mcw_widget_list_append(McwWidgetList* list, McwWidget* mcw_widget); 
McwWidget* mcw_widget_list_get(McwWidgetList* list, int index);
void mcw_widget_list_set(McwWidgetList* list, int index, McwWidget* mcw_widget); 
void mcw_widget_list_remove(McwWidgetList* list, int index);

unsigned long mcw_widget_list_len(McwWidgetList* list);

#endif //POINT_LIST_H
