#ifndef ARKANOID_PLATFORM_H
#define ARKANOID_PLATFORM_H

#include "McwWidget.h"

#define PLATFORM_IMAGE "icons/panel.png"
#define LONG_PLATFORM_IMAGE "icons/panel_long.png"

#define PLATFORM_HEIGHT_POSITION 560
#define PLATFORM_WIDTH 90
#define LONG_PLATFORM_WIDTH 120
#define PLATFORM_HEIGT 15

typedef struct _platform Platform;

Platform* platform_new();
void platform_make_long(Platform* platform);
void platform_make_normal(Platform* platform);

#endif // PLATFORM_H_INCLUDED
