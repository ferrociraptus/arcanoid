#ifndef RVECTORSTRUCT_H_INCLUDED
#define RVECTORSTRUCT_H_INCLUDED

#include "PointStruct.h"

typedef Point RVector;

RVector rvector_create(double x, double y);
double rvector_len(RVector vector);
RVector rvector_sum(RVector vector1, RVector vector2);
RVector rvector_multiply(RVector vector, double val);
RVector rvector_set_len(RVector vector, double len);
int rvector_is_zero(RVector vector);

#endif // RVECTORSTRUCT_H_INCLUDED
