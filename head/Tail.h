#ifndef TAIL_NODE_H
#define TAIL_NODE_H

#include "ColorRangeStruct.h"
#include "PointStruct.h"
#include "VoidList.h"

typedef struct{
    Point position;
    ColorRGB color;
    double radius;
} TailNode;

typedef VoidList Tail;

Tail* tail_new(unsigned length, Point start_position, ColorRange colors, double radius, double measure_diff);
void tail_shift_head(Tail* tail, Point vector);
void tail_change_color_range(Tail* tail, ColorRange colors);
void tail_destroy(Tail* tail);
void tail_print(Tail* tail);

#endif
