#ifndef COLOR_RANGE
#define COLOR_RANGE

#include "ColorRGBStruct.h"

typedef struct _color_range{
    ColorRGB first;
    ColorRGB second;
    unsigned range;
} ColorRange;

ColorRange color_range_create(ColorRGB first, ColorRGB second);
void color_range_set_range(ColorRange* color_range, unsigned range);
ColorRGB color_range_get_val(ColorRange range, unsigned iter);


#endif
