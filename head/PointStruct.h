#ifndef POINT_H
#define POINT_H

typedef struct{
    double x;
    double y;
} Point;

Point point_create(double x, double y);
Point point_sum(Point point1, Point point2);
Point point_reduce(Point point1, Point point2);
int point_is_equal(Point point1, Point point2);
int point_is_zero(Point point);

#endif
