#ifndef MCW_SYSTEM_H
#define MCW_SYSTEM_H

#include "mcw.h"

#define MIN_WINDOW_SIZE_WEIGHT_PX 708
#define MIN_WINDOW_SIZE_HEIGHT_PX 600

#define BACKGROUND_COLOR 0.7, 0.7, 0.7

// #define REDRAW_SYSTEM_TIME 20
#define TIMER_TICK_VAL 1

void mcw_system_connect_init_function(void (*initialization_fun)());
void mcw_system_run(int argc, char** argv);
void mcw_system_load_ui(const char* file_name);
void mcw_system_new_ui();
void mcw_system_close_ui();
void mcw_system_close();

McwWidget* mcw_system_get_widget(const char* indificator);
void mcw_system_add_widget(McwWidget* widget);

#endif
